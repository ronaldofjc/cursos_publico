const express = require('express');
const nunjucks = require('nunjucks');
const path = require('path');
const bodyParser = require('body-parser');
const moment = require('moment');

const app = express();

nunjucks.configure('views', {
  autoescape: true,
  express: app,
});

app.set('view engine', 'njk');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.render('main');
});

app.post('/check', (req, res) => {
  const { name, birth } = req.body;
  const age = moment().diff(moment(birth, 'YYYY/MM/DD'), 'years');
  if (age < 18) {
    res.redirect(`minor?nome=${name}`);
  } else {
    res.redirect(`major?nome=${name}`);
  }
});

const validateName = (req, res, next) => {
  if (req.query.nome) {
    next();
  } else {
    res.redirect('/');
  }
};

app.get('/major?:name', validateName, (req, res) => {
  const name = req.query.nome;
  res.render('major', { name });
});

app.get('/minor', validateName, (req, res) => {
  const name = req.query.nome;
  res.render('minor', { name });
});

app.listen(3000, () => {
  console.log('Running...');
});
