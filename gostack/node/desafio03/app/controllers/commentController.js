const mongoose = require('mongoose');

const Post = mongoose.model('Post');
const Comment = mongoose.model('Comment');

module.exports = {
  async create(req, res, next) {
    try {
      const post = await Post.findById(req.params.postId).populate('comments');
      const comment = await Comment.create({ ...req.body, user: req.userId });

      if (!post) {
        return res.status(400).json({ error: 'Post does not exist' });
      }

      if (!comment) {
        return res.status(400).json({ error: 'Comment does not exist' });
      }

      post.comments.push(comment);
      await post.save();

      return res.json(post);
    } catch (err) {
      return next(err);
    }
  },
  async destroy(req, res, next) {
    try {
      const post = await Post.findById(req.params.postId);
      const comment = await Comment.findById(req.params.id);

      if (!post) {
        return res.status(400).json({ error: 'Post does not exist' });
      }

      if (!comment) {
        return res.status(400).json({ error: 'Comment does not exist' });
      }

      await comment.remove();

      post.comments.splice(post.comments.indexOf(req.params.id), 1);
      await post.save();

      return res.json('Comentário removido!');
    } catch (err) {
      return next(err);
    }
  },
};
