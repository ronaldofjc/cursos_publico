const express = require('express');
const requireDir = require('require-dir');

const routes = express.Router();

const authMiddleware = require('./middlewares/auth');

const controllers = requireDir('./controllers');

// Auth
routes.post('/signin', controllers.authController.signin);
routes.post('/signup', controllers.authController.signup);

routes.use(authMiddleware);

// User
routes.get('/users/me', controllers.userController.me);
routes.put('/users', controllers.userController.update);
routes.get('/feed', controllers.userController.feed);

// Friends
routes.post('/friend/:id', controllers.friendController.create);
routes.delete('/notfriend/:id', controllers.friendController.destroy);

// Comments
routes.post('/post/:postId/comment/create', controllers.commentController.create);
routes.delete('/post/:postId/comment/:id', controllers.commentController.destroy);

// Posts
routes.post('/post', controllers.postController.create);
routes.delete('/post/:id', controllers.postController.destroy);

// Likes
routes.post('/like/:id', controllers.likeController.toggle);

module.exports = routes;
