const path = require('path');

module.exports = {
  url: 'mongodb://localhost:27017/goNodeDesafio03',

  modelsPath: path.resolve('app', 'models'),
};
