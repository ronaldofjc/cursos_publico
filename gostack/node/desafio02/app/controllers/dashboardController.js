const { Project, Section } = require('../models');

module.exports = {
  async index(req, res, next) {
    try {
      const { user } = req.session;
      const projects = await Project.findAll({
        include: [Section],
        where: {
          UserId: req.session.user.id,
        },
      });
      const quantityProjects = projects.length;
      return res.render('dashboard/index', { projects, quantityProjects, user });
    } catch (err) {
      return next(err);
    }
  },
};
