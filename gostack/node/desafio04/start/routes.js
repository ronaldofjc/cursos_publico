'use strict'

const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('users', 'UserController.store').validator('UserStore')

Route.post('sessions', 'SessionController.store').validator('Session')

Route.post('passwords', 'ForgotPasswordController.store').validator('ForgotPassword')
Route.put('passwords', 'ForgotPasswordController.update').validator('ResetPassword')

Route.group(() => {
  Route.put('users/:id', 'UserController.update').validator('UserUpdate')
  Route.get('users/:id', 'UserController.index')

  Route.resource('events', 'EventController')
    .apiOnly()
    .validator(new Map([[['events.store', 'events.update'], ['Event']]]))

  Route.post('events/:id/share', 'ShareController.create').validator('Share')
}).middleware(['auth'])
