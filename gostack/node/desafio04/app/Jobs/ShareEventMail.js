'use strict'

const Mail = use('Mail')

class ShareEventMail {
  // If this getter isn't provided, it will default to 1.
  // Increase this number to increase processing concurrency.
  static get concurrency () {
    return 1
  }

  // This is required. This is a unique key used to identify this job.
  static get key () {
    return 'ShareEventMail-job'
  }

  // This is where the work is done.
  async handle ({ recipientEmail, username, ...data }) {
    try {
      await Mail.send(['emails.share_event'], { username, ...data }, message => {
        message
          .to(recipientEmail)
          .from('ronaldo@atlantisweb.com', 'Share Event')
          .subject(`${username} compartilhou um evento com você`)
      })
    } catch (err) {
      console.log(err)
    }
  }
}

module.exports = ShareEventMail
