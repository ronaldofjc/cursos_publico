'use strict'

const Kue = use('Kue')
const Job = use('App/Jobs/ShareEventMail')

const Event = use('App/Models/Event')

const moment = require('moment')

/**
 * Resourceful controller for interacting with shares
 */
class ShareController {
  /**
   * Render a form to be used for creating a new share.
   * GET shares/create
   */
  async create ({ request, response, params, auth }) {
    const event = await Event.findOrFail(params.id)

    if (event.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    const recipientEmail = request.input('recipient_email')

    const { username, email } = auth.user
    const { title, location } = event

    const date = moment(event.date).format('DD/MM/YYYY')
    const time = moment(event.date).format('HH:mm:ss')

    Kue.dispatch(
      Job.key,
      { recipientEmail, username, email, title, location, date, time },
      {
        attempts: 5
      }
    )

    return response.json('Share Ok!')
  }
}

module.exports = ShareController
