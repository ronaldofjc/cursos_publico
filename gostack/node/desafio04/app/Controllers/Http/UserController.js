'use strict'

const User = use('App/Models/User')
const Hash = use('Hash')

class UserController {
  async index ({ params, response }) {
    try {
      const user = await User.findOrFail(params.id)

      return user
    } catch (err) {
      return response
        .status(err.status)
        .send({ error: { message: 'User not found!' } })
    }
  }

  async store ({ request }) {
    const data = request.only(['username', 'email', 'password'])

    const user = await User.create(data)

    return user
  }

  async update ({ params, request, response }) {
    const user = await User.findOrFail(params.id)
    const { username, oldPassword, password } = request.all()

    if (oldPassword && password) {
      const validPassword = await Hash.verify(oldPassword, user.password)

      if (!validPassword) {
        return response
          .status(401)
          .send({ error: { message: 'Current Password is invalid!' } })
      }
      user.password = password
    }

    user.username = username

    await user.save()

    return user
  }
}

module.exports = UserController
