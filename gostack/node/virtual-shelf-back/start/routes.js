'use strict'

const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('users', 'UserController.store').validator('User')
Route.get('users/:id', 'UserController.index')
Route.put('users/:id', 'UserController.update')

Route.post('sessions', 'SessionController.store').validator('Session')

Route.group(() => {
  Route.resource('comics', 'ComicController')
    .apiOnly()
    .validator(new Map([[['comics.store', 'comics.update'], ['Comic']]]))

  Route.resource('books', 'BookController')
    .apiOnly()
    .validator(new Map([[['books.store', 'books.update'], ['Book']]]))

  Route.resource('mangas', 'MangaController')
    .apiOnly()
    .validator(new Map([[['mangas.store', 'mangas.update'], ['Manga']]]))
}).middleware(['auth'])
