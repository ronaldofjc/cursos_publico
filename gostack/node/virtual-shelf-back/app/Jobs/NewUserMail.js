'use strict'

const Mail = use('Mail')

class NewUserMail {
  // If this getter isn't provided, it will default to 1.
  // Increase this number to increase processing concurrency.
  static get concurrency () {
    return 1
  }

  // This is required. This is a unique key used to identify this job.
  static get key () {
    return 'NewUserMail-job'
  }

  // This is where the work is done.
  async handle ({ username, email }) {
    console.log(`Job: ${NewUserMail.key}`)

    await Mail.send(
      ['emails.create_user'],
      { username, email },
      message => {
        message
          .to(email)
          .from('ronaldo@atlantisweb.com.br', 'Ronaldo | Atlantis')
          .subject('Novo usuário criado - Virtual Shelf!')
      }
    )
  }
}

module.exports = NewUserMail

