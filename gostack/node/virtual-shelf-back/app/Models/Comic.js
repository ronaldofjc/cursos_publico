'use strict'

const Model = use('Model')

class Comic extends Model {
  user () {
    return this.belongsTo('App/Models/User')
  }
}

module.exports = Comic
