'use strict'

const Kue = use('Kue')
const Job = use('App/Jobs/NewUserMail')

const UserHook = exports = module.exports = {}

UserHook.sendNewUserMail = async userInstance => {
    const { username, email } = await userInstance
    
    Kue.dispatch(Job.key, { username, email }, { attempts: 3 })
}
