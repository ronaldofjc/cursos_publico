'use strict'

const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    this.addHook('beforeCreate', 'UserHook.sendNewUserMail')
  }

  tokens () {
    return this.hasMany('App/Models/Token')
  }

  comics () {
    return this.hasMany('App/Models/Comic')
  }
}

module.exports = User
