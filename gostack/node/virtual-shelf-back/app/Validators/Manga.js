'use strict'

const Antl = use('Antl')

class Manga {
  get validateAll () {
    return true
  }
  
  get rules () {
    return {
      title: 'required',
      publishingCompany: 'required'
    }
  }

  get messages () {
    return Antl.list('validation')
  }
}

module.exports = Manga
