'use strict'

const Comic = use('App/Models/Comic')

class ComicController {
  /**
   * Show a list of all comics.
   * GET comics
   */
  async index ({ auth, request }) {
    const { page } = request.get()

    const comics = await Comic.query()
      .where('user_id', auth.user.id)
      .orderBy('created_at', 'desc')
      .paginate(page)

    return comics
  }

  /**
   * Create/save a new comic.
   * POST comics
   */
  async store ({ request, auth }) {
    const data = request.only([
      'title', 'subtitle', 'serie', 'publishingCompany', 'edition', 'pages',
      'evaluation', 'cover', 'type', 'read', 'writer', 'artist'
    ])

    const comic = await Comic.create({ ...data, user_id: auth.user.id })

    return comic
  }

  /**
   * Display a single comic.
   * GET comics/:id
   */
  async show ({ params, response, auth }) {
    const comic = await Comic.findOrFail(params.id)

    if (comic.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    return comic
  }

  /**
   * Update comic details.
   * PUT or PATCH comics/:id
   */
  async update ({ params, request, response, auth }) {
    const comic = await Comic.findOrFail(params.id)
    const data = request.only([
      'title', 'subtitle', 'serie', 'publishingCompany', 'edition', 'pages',
      'evaluation', 'cover', 'type', 'read', 'writer', 'artist'
    ])

    if (comic.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    comic.merge(data)

    await comic.save()

    return comic
  }

  /**
   * Delete a comic with id.
   * DELETE comics/:id
   */
  async destroy ({ params, response, auth }) {
    const comic = await Comic.findOrFail(params.id)

    if (comic.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    comic.delete()
    return response.json('Comic removed!')
  }
}

module.exports = ComicController
