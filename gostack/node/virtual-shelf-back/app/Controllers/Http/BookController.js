'use strict'

const Book = use('App/Models/Book')

class BookController {
  /**
   * Show a list of all books.
   * GET books
   */
  async index ({ auth, request }) {
    const { page } = request.get()

    const books = await Book.query()
      .where('user_id', auth.user.id)
      .orderBy('created_at', 'desc')
      .paginate(page)

    return books
  }

  /**
   * Create/save a new book.
   * POST books
   */
  async store ({ request, auth }) {
    const data = request.only([
      'title', 'subtitle', 'publishingCompany', 'pages', 'evaluation',
      'cover', 'read', 'writer'
    ])

    const book = await Book.create({ ...data, user_id: auth.user.id })

    return book
  }

  /**
   * Display a single book.
   * GET books/:id
   */
  async show ({ params, response, auth }) {
    const book = await Book.findOrFail(params.id)

    if (book.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    return book
  }

  /**
   * Update book details.
   * PUT or PATCH books/:id
   */
  async update ({ params, request, response, auth }) {
    const book = await Book.findOrFail(params.id)
    const data = request.only([
      'title', 'subtitle', 'publishingCompany', 'pages', 'evaluation',
      'cover', 'read', 'writer'
    ])

    if (book.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    book.merge(data)

    await book.save()

    return book
  }

  /**
   * Delete a book with id.
   * DELETE books/:id
   */
  async destroy ({ params, response, auth }) {
    const book = await Book.findOrFail(params.id)

    if (book.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    book.delete()
    return response.json('Book removed!')
  }
}

module.exports = BookController
