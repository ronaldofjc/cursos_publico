'use strict'

const Manga = use('App/Models/Manga')

/**
 * Resourceful controller for interacting with manga
 */
class MangaController {
  /**
   * Show a list of all manga.
   * GET manga
   */
  async index ({ auth, request }) {
    const { page } = request.get()

    const mangas = await Manga.query()
      .where('user_id', auth.user.id)
      .orderBy('created_at', 'desc')
      .paginate(page)

    return mangas
  }

  /**
   * Create/save a new manga.
   * POST manga
   */
  async store ({ request, auth }) {
    const data = request.only([
      'title', 'publishingCompany', 'edition', 'pages', 'evaluation',
      'cover', 'read', 'writer', 'artist'
    ])

    const manga = await Manga.create({ ...data, user_id: auth.user.id })

    return manga
  }

  /**
   * Display a single manga.
   * GET manga/:id
   */
  async show ({ params, response, auth }) {
    const manga = await Manga.findOrFail(params.id)

    if (manga.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    return manga
  }

  /**
   * Update manga details.
   * PUT or PATCH manga/:id
   */
  async update ({ params, request, response, auth }) {
    const manga = await Manga.findOrFail(params.id)
    const data = request.only([
      'title', 'publishingCompany', 'edition', 'pages', 'evaluation',
      'cover', 'read', 'writer', 'artist'
    ])

    if (manga.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    manga.merge(data)

    await manga.save()

    return manga
  }

  /**
   * Delete a manga with id.
   * DELETE manga/:id
   */
  async destroy ({ params, response, auth }) {
    const manga = await Manga.findOrFail(params.id)

    if (manga.user_id !== auth.user.id) {
      return response
        .status(401)
        .send({ error: { message: 'Permission denied' } })
    }

    manga.delete()
    return response.json('Manga removed!')
  }
}

module.exports = MangaController
