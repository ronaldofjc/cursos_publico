'use strict'

const Schema = use('Schema')

class MangaSchema extends Schema {
  up () {
    this.create('manga', (table) => {
      table.increments()
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.string('title').notNullable()
      table.string('publishingCompany').notNullable()
      table.integer('edition')
      table.integer('pages')
      table.integer('evaluation')
      table.string('cover')
      table.string('read')
      table.string('writer')
      table.string('artist')
      table.timestamps()
    })
  }

  down () {
    this.drop('manga')
  }
}

module.exports = MangaSchema
