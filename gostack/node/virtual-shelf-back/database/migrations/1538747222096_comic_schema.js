'use strict'

const Schema = use('Schema')

class ComicSchema extends Schema {
  up () {
    this.create('comics', (table) => {
      table.increments()
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.string('title').notNullable()
      table.string('subtitle')
      table.string('serie')
      table.string('publishingCompany').notNullable()
      table.integer('edition')
      table.integer('pages')
      table.integer('evaluation')
      table.string('cover')
      table.string('type')
      table.string('read')
      table.string('writer')
      table.string('artist')
      table.timestamps()
    })
  }

  down () {
    this.drop('comics')
  }
}

module.exports = ComicSchema
