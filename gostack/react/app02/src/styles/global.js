import { createGlobalStyle } from 'styled-components';

import 'font-awesome/css/font-awesome.css';

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  h1 {
    color: #fff;
  }

  body {
    background: #9B65E5;
    text-rendering: optimizeLegibility !important;
    -webkit-font-smothing: antialiased !important;
    font-family: sans-serif;
  }
`;
