import React, { Component } from 'react';
import PropTypes from 'prop-types';
import api from '../../services/api';

import { Container, RepositoryItem } from './styles';

export default class Sidebar extends Component {
  static propTypes = {
    handleSelectRepository: PropTypes.func.isRequired,
  };

  state = {
    repositories: [],
    newRepository: '',
    activeRepositoryId: {},
  };

  handleInputChange = (e) => {
    this.setState({ newRepository: e.target.value });
  };

  handleAddRepository = async (e) => {
    e.preventDefault();

    const { repositories, newRepository } = this.state;

    const response = await api.get(`/repos/${newRepository}`);

    // Não permite repositórios duplicados
    if (repositories.find(repository => repository.id === response.data.id)) {
      return false;
    }

    this.setState({ repositories: [...repositories, response.data] });
  };

  handleClickRepository = (repository) => {
    const { handleSelectRepository } = this.props;

    this.setState({ activeRepositoryId: repository.id });

    handleSelectRepository(repository);
  };

  render() {
    const { repositories, newRepository, activeRepositoryId } = this.state;

    return (
      <Container>
        <form onSubmit={this.handleAddRepository}>
          <input
            type="text"
            placeholder="usuário/repositório"
            value={newRepository}
            onChange={this.handleInputChange}
          />
          <button type="submit">
            <i className="fa fa-plus-circle" />
          </button>
        </form>

        <div>
          {repositories.map(repository => (
            <RepositoryItem
              active={repository.id === activeRepositoryId}
              type="button"
              onClick={() => this.handleClickRepository(repository)}
            >
              <img
                src={repository.owner.avatar_url}
                alt={repository.full_name}
              />
              <div>
                <strong>{repository.name}</strong>
                <span>{repository.owner.login}</span>
              </div>
            </RepositoryItem>
          ))}
        </div>
      </Container>
    );
  }
}
