import React, { Component } from 'react';

import Sidebar from '../../components/Sidebar';
import Issues from '../../components/Issues';

import { Container } from './styles';

export default class Main extends Component {
  state = {
    activeRepository: null,
  };

  handleSelectRepository = (repository) => {
    this.setState({ activeRepository: repository });
  };

  render() {
    const { activeRepository } = this.state;

    return (
      <Container>
        <Sidebar handleSelectRepository={this.handleSelectRepository} />
        {activeRepository && <Issues repository={activeRepository} />}
      </Container>
    );
  }
}
