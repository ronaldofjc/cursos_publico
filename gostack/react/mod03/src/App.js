import React from 'react';
import { Provider } from 'react-redux';
import Main from './pages/Main';
import store from './store';

const App = () => (
  <React.Fragment>
    <Provider store={store}>
      <Main />
    </Provider>
  </React.Fragment>
);

export default App;
